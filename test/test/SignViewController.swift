//
//  ViewController.swift
//  test
//
//  Created by Propio Data on 6/30/19.
//  Copyright © 2019 PedroDaza. All rights reserved.
//

import UIKit
import FirebaseUI
import Firebase
class SignViewController: UIViewController, FUIAuthDelegate {
    
    // You need to adopt a FUIAuthDelegate protocol to receive callback
    var FUIAUTH:FUIAuth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseApp.configure()
        
        let AUTH = FUIAuth.defaultAuthUI()
        AUTH?.delegate = self
        
        let providers: [FUIAuthProvider] = [
            FUIGoogleAuth(),
            FUIEmailAuth()
        ]
        AUTH?.providers = providers
        
        
        FUIAUTH = AUTH
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let user = FUIAUTH!.auth!.currentUser
        
        if user != nil{
            self.performSegue(withIdentifier: "showTabs", sender: nil)
        }else{
            let authViewController = FUIAUTH?.authViewController()
            
            present(authViewController!, animated: true, completion: nil)
        }
        
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
    }


}

